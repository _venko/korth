package dictionary

import instruction.Instruction
import instruction.Op
import types.VmWord
import types.WordAddr

data class Word(
    val name: String,
    val code: MutableList<Instruction>,
    val data: MutableList<VmWord>,
    val immediate: Boolean,
    var hidden: Boolean
) {
    companion object Init {
        fun new(
            name: String,
            code: MutableList<Instruction>
        ) = Word(name, code, mutableListOf(), false, false)

        fun newImmediate(
            name: String,
            code: MutableList<Instruction>
        ) = Word(name, code, mutableListOf(), true, false)

        fun op(
            name: String,
            op: Op
        ) = Word(
            name,
            mutableListOf(Instruction.native(op), Instruction.native(Op.Return())),
            mutableListOf(),
            false,
            false
        )

        fun opImmediate(
            name: String,
            op: Op
        ) = Word(
            name,
            mutableListOf(Instruction.native(op), Instruction.native(Op.Return())),
            mutableListOf(),
            true,
            false
        )
    }
}

class Dictionary {
    val words = ArrayList<Word>()

    init {
        words.add(Word.op("bye", Op.Shutdown()))
        words.add(Word.op("return", Op.Return()))
        words.add(Word.op(".", Op.Dot()))
        words.add(Word.op("type", Op.Type()))
        words.add(Word.op(".s", Op.DotS()))
        words.add(Word.op("drop", Op.Drop()))
        words.add(Word.op("swap", Op.Swap()))
        words.add(Word.op("dup", Op.Dup()))
        words.add(Word.op("rot", Op.Rot()))
        words.add(Word.op("over", Op.Over()))
        words.add(Word.op("+", Op.Add()))
        words.add(Word.op("-", Op.Sub()))
        words.add(Word.op("*", Op.Mul()))
        words.add(Word.op("/", Op.Div()))
        words.add(Word.op("%", Op.Mod()))
        words.add(Word.op("**", Op.Pow()))
        words.add(Word.op("=", Op.Eq()))
        words.add(Word.op("0=", Op.EqZero()))
        words.add(Word.op("<", Op.LessThan()))
        words.add(Word.op("<=", Op.LessThanEq()))
        words.add(Word.op(">", Op.GreaterThan()))
        words.add(Word.op(">=", Op.GreaterThanEq()))
        words.add(Word.op("not", Op.Not()))
        words.add(Word.op("and", Op.And()))
        words.add(Word.op("or", Op.Or()))
        words.add(Word.op("<<", Op.Shl()))
        words.add(Word.op(">>", Op.Shr()))
        words.add(Word.op("~", Op.BinNot()))
        words.add(Word.op("&", Op.BinAnd()))
        words.add(Word.op("|", Op.BinOr()))
        words.add(Word.op("^", Op.BinXor()))
        words.add(Word.op(":", Op.Define()))
        words.add(Word.opImmediate(";", Op.EndDefine()))
        words.add(Word.opImmediate("[", Op.InterpretMode()))
        words.add(Word.opImmediate("]", Op.EndInterpretMode()))
        words.add(Word.opImmediate("literal", Op.LiteralFromStack()))
        words.add(
            Word.newImmediate(
                "]l",
                mutableListOf(
                    Instruction.native(Op.EndInterpretMode()),
                    Instruction.native(Op.LiteralFromStack()),
                    Instruction.native(Op.Return())
                )
            )
        )
        words.add(Word.op("word", Op.ParseWord()))
        words.add(Word.op("find", Op.Find()))
        words.add(Word.opImmediate("if", Op.PlaceJumpMarker()))
        words.add(Word.opImmediate("then", Op.FillJumpMarker()))
    }

    fun lookupWord(name: String): WordAddr? {
        return this.words.withIndex().reversed().find { (_, word) -> word.name == name }?.index
    }
}
