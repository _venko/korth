package parser

class Parser(val input: String) {
    val chars = input.toCharArray()
    var cursor = 0

    fun atEoi(): Boolean = this.cursor == this.input.length

    fun skipWhitespace() {
        while (!this.atEoi() && this.chars[this.cursor].isWhitespace()) {
            this.cursor += 1
        }
    }

    fun consumeWord(): String? {
        this.skipWhitespace()

        val startIndex = this.cursor
        while (!this.atEoi() && !this.chars[this.cursor].isWhitespace()) {
            this.cursor += 1
        }

        if (startIndex == this.cursor) {
            return null
        } else {
            return input.subSequence(startIndex, this.cursor).toString()
        }
    }
}
