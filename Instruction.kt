package instruction

import types.VmWord
import types.WordAddr

sealed class Op {
    class Shutdown : Op()

    class Return : Op()

    class Dot : Op()

    class Type : Op()

    class DotS : Op()

    class Drop : Op()

    class Swap : Op()

    class Dup : Op()

    class Rot : Op()

    class Over : Op()

    class Add : Op()

    class Sub : Op()

    class Mul : Op()

    class Div : Op()

    class Mod : Op()

    class Pow : Op()

    class Eq : Op()

    class EqZero : Op()

    class LessThan : Op()

    class LessThanEq : Op()

    class GreaterThan : Op()

    class GreaterThanEq : Op()

    class Not : Op()

    class And : Op()

    class Or : Op()

    class Shl : Op()

    class Shr : Op()

    class BinNot : Op()

    class BinAnd : Op()

    class BinOr : Op()

    class BinXor : Op()

    class PlaceJumpMarker : Op()

    class FillJumpMarker : Op()

    sealed class JumpOp : Op() {
        class AwaitingOffset : JumpOp()

        data class JumpIfZero(val offset: Int) : JumpOp()
    }

    class Here : Op()

    data class Literal(val value: VmWord) : Op()

    class LiteralFromStack : Op()

    class Define : Op()

    class EndDefine : Op()

    class InterpretMode : Op()

    class EndInterpretMode : Op()

    class ParseWord : Op()

    class Find : Op()
}

sealed class Instruction {
    data class Native(val op: Op) : Instruction()

    data class User(val addr: WordAddr) : Instruction()

    companion object Init {
        fun native(op: Op): Instruction = Native(op)

        fun user(addr: WordAddr): Instruction = User(addr)
    }
}
