package machine

import dictionary.Dictionary
import dictionary.Word
import instruction.Instruction
import instruction.Op
import parser.Parser
import stack.Stack
import types.CodePtr
import types.VmWord
import types.WordAddr
import kotlin.math.pow
import kotlin.text.toIntOrNull

enum class VmMode {
    INTERPRETING,
    COMPILING,
}

enum class VmError {
    STACK_UNDERFLOW,
    INVALID_STRING_INDEX,
    UNKNOWN_WORD,
    ZERO_LENGTH_WORD,
    COMPILE_ONLY_WORD,
    UNREACHABLE_WORD,
}

class Vm() {
    val dict = Dictionary()
    val stack = Stack<VmWord>()
    val returnStack = Stack<CodePtr>()
    var codePtr = CodePtr.default()
    var mode = VmMode.INTERPRETING
    var latestWordAddr: WordAddr? = null
    var parser: Parser = Parser("")

    private fun showPrompt() {
        print("> ")
        System.out.flush()
    }

    private fun getUserInput(): Parser {
        while (true) {
            showPrompt()
            val input = readLine()
            if (!input.isNullOrEmpty()) {
                return Parser(input)
            }
        }
    }

    private fun inputExhausted(): Boolean {
        parser.skipWhitespace()
        return parser.atEoi()
    }

    fun execute() {
        while (true) {
            val instr = dict.words[codePtr.addr].code[codePtr.offset]
            if (instr is Instruction.User) {
                returnStack.push(codePtr)
                codePtr = CodePtr.startOf(instr.addr)
                continue
            } else if (instr is Instruction.Native) {
                when (instr.op) {
                    is Op.Shutdown -> {}
                    is Op.Return -> {
                        if (returnStack.isEmpty()) {
                            return
                        }
                        codePtr = returnStack.pop()!!
                    }
                    is Op.Dot -> {
                        if (stack.size() < 1) {
                            throw Exception("STACK UNDERFLOW")
                        }
                        println(stack.pop())
                    }
                    is Op.Type -> {}
                    is Op.DotS -> {
                        val count = stack.size()
                        val items = stack.items.reversed()
                        println("<$count> $items")
                    }
                    is Op.Drop -> {
                        if (stack.size() < 1) {
                            throw Exception("STACK UNDERFLOW")
                        }
                        stack.pop()
                    }
                    is Op.Swap -> {
                        if (stack.size() < 2) {
                            throw Exception("STACK UNDERFLOW")
                        }

                        val rhs = stack.pop()!!
                        val lhs = stack.pop()!!
                        stack.push(rhs)
                        stack.push(lhs)
                    }
                    is Op.Dup -> {
                        if (stack.size() < 1) {
                            throw Exception("STACK UNDERFLOW")
                        }

                        stack.push(stack.top()!!)
                    }
                    is Op.Rot -> {
                        if (stack.size() < 3) {
                            throw Exception("STACK UNDERFLOW")
                        }

                        val rhs = stack.pop()!!
                        val mid = stack.pop()!!
                        val lhs = stack.pop()!!
                        stack.push(mid)
                        stack.push(rhs)
                        stack.push(lhs)
                    }
                    is Op.Over -> {
                        if (stack.size() < 2) {
                            throw Exception("STACK UNDERFLOW")
                        }

                        val rhs = stack.pop()!!
                        val lhs = stack.pop()!!
                        stack.push(lhs)
                        stack.push(rhs)
                        stack.push(lhs)
                    }
                    is Op.Add -> {
                        if (stack.size() < 2) {
                            throw Exception("STACK UNDERFLOW")
                        }

                        val rhs = stack.pop()!!
                        val lhs = stack.pop()!!
                        stack.push(lhs + rhs)
                    }
                    is Op.Sub -> {
                        if (stack.size() < 2) {
                            throw Exception("STACK UNDERFLOW")
                        }

                        val rhs = stack.pop()!!
                        val lhs = stack.pop()!!
                        stack.push(lhs - rhs)
                    }
                    is Op.Mul -> {
                        if (stack.size() < 2) {
                            throw Exception("STACK UNDERFLOW")
                        }

                        val rhs = stack.pop()!!
                        val lhs = stack.pop()!!
                        stack.push(lhs * rhs)
                    }
                    is Op.Div -> {
                        if (stack.size() < 2) {
                            throw Exception("STACK UNDERFLOW")
                        }

                        val rhs = stack.pop()!!
                        val lhs = stack.pop()!!
                        stack.push(lhs / rhs)
                    }
                    is Op.Mod -> {
                        if (stack.size() < 2) {
                            throw Exception("STACK UNDERFLOW")
                        }

                        val rhs = stack.pop()!!
                        val lhs = stack.pop()!!
                        stack.push(lhs % rhs)
                    }
                    is Op.Pow -> {
                        if (stack.size() < 2) {
                            throw Exception("STACK UNDERFLOW")
                        }

                        val rhs = stack.pop()!!.toDouble()
                        val lhs = stack.pop()!!.toDouble()
                        stack.push(lhs.pow(rhs).toInt())
                    }
                    is Op.Eq -> {
                        if (stack.size() < 2) {
                            throw Exception("STACK UNDERFLOW")
                        }

                        val rhs = stack.pop()!!.toDouble()
                        val lhs = stack.pop()!!.toDouble()
                        val result = if (lhs == rhs) -1 else 0
                        stack.push(result)
                    }
                    is Op.EqZero -> {
                        if (stack.size() < 1) {
                            throw Exception("STACK UNDERFLOW")
                        }

                        val top = stack.pop()!!
                        val result = if (top == 0) -1 else 0
                        stack.push(result)
                    }
                    is Op.LessThan -> {
                        if (stack.size() < 2) {
                            throw Exception("STACK UNDERFLOW")
                        }

                        val rhs = stack.pop()!!
                        val lhs = stack.pop()!!
                        val result = if (lhs < rhs) -1 else 0
                        stack.push(result)
                    }
                    is Op.LessThanEq -> {
                        if (stack.size() < 2) {
                            throw Exception("STACK UNDERFLOW")
                        }

                        val rhs = stack.pop()!!
                        val lhs = stack.pop()!!
                        val result = if (lhs <= rhs) -1 else 0
                        stack.push(result)
                    }
                    is Op.GreaterThan -> {
                        if (stack.size() < 2) {
                            throw Exception("STACK UNDERFLOW")
                        }

                        val rhs = stack.pop()!!
                        val lhs = stack.pop()!!
                        val result = if (lhs > rhs) -1 else 0
                        stack.push(result)
                    }
                    is Op.GreaterThanEq -> {
                        if (stack.size() < 2) {
                            throw Exception("STACK UNDERFLOW")
                        }

                        val rhs = stack.pop()!!
                        val lhs = stack.pop()!!
                        val result = if (lhs >= rhs) -1 else 0
                        stack.push(result)
                    }
                    is Op.Not -> {
                        if (stack.size() < 1) {
                            throw Exception("STACK UNDERFLOW")
                        }

                        val top = stack.pop()!! != 0
                        val result = if (!top) -1 else 0
                        stack.push(result)
                    }
                    is Op.And -> {
                        if (stack.size() < 2) {
                            throw Exception("STACK UNDERFLOW")
                        }

                        val rhs = stack.pop()!! != 0
                        val lhs = stack.pop()!! != 0
                        val result = if (rhs && lhs) -1 else 0
                        stack.push(result)
                    }
                    is Op.Or -> {
                        if (stack.size() < 2) {
                            throw Exception("STACK UNDERFLOW")
                        }

                        val rhs = stack.pop()!! != 0
                        val lhs = stack.pop()!! != 0
                        val result = if (rhs || lhs) -1 else 0
                        stack.push(result)
                    }
                    is Op.Shl -> {
                        if (stack.size() < 2) {
                            throw Exception("STACK UNDERFLOW")
                        }

                        val rhs = stack.pop()!!
                        val lhs = stack.pop()!!
                        stack.push(lhs shl rhs)
                    }
                    is Op.Shr -> {
                        if (stack.size() < 2) {
                            throw Exception("STACK UNDERFLOW")
                        }

                        val rhs = stack.pop()!!
                        val lhs = stack.pop()!!
                        stack.push(lhs shr rhs)
                    }
                    is Op.BinNot -> {
                        if (stack.size() < 1) {
                            throw Exception("STACK UNDERFLOW")
                        }

                        stack.push(stack.pop()!!.inv())
                    }
                    is Op.BinAnd -> {
                        if (stack.size() < 2) {
                            throw Exception("STACK UNDERFLOW")
                        }

                        val rhs = stack.pop()!!
                        val lhs = stack.pop()!!
                        stack.push(lhs and rhs)
                    }
                    is Op.BinOr -> {
                        if (stack.size() < 2) {
                            throw Exception("STACK UNDERFLOW")
                        }

                        val rhs = stack.pop()!!
                        val lhs = stack.pop()!!
                        stack.push(lhs or rhs)
                    }
                    is Op.BinXor -> {
                        if (stack.size() < 2) {
                            throw Exception("STACK UNDERFLOW")
                        }

                        val rhs = stack.pop()!!
                        val lhs = stack.pop()!!
                        stack.push(lhs xor rhs)
                    }
                    is Op.PlaceJumpMarker -> {
                        if (mode != VmMode.COMPILING) {
                            throw Exception("COMPILE ONLY WORD")
                        }

                        val latestAddr = latestWordAddr!!
                        val latestExecToken = dict.words[latestAddr].code
                        latestExecToken.add(Instruction.native(Op.JumpOp.AwaitingOffset()))
                    }
                    is Op.FillJumpMarker -> {
                        if (mode != VmMode.COMPILING) {
                            throw Exception("COMPILE ONLY WORD")
                        }

                        val latestAddr = latestWordAddr!!
                        val currentXtIndex = dict.words[latestAddr].code.size - 1
                        var offset = 0
                        while (true) {
                            val cursor = dict.words[latestAddr].code[currentXtIndex - offset]
                            if (cursor is Instruction.Native && cursor.op is Op.JumpOp.AwaitingOffset) {
                                dict.words[latestAddr].code[currentXtIndex - offset] = Instruction.native(Op.JumpOp.JumpIfZero(offset))
                                break
                            }
                            offset += 1
                        }
                    }
                    is Op.JumpOp.AwaitingOffset -> {
                        throw Exception("UNREACHABLE")
                    }
                    is Op.JumpOp.JumpIfZero -> {
                        if (stack.size() < 1) {
                            throw Exception("STACK UNDERFLOW")
                        }

                        if (stack.pop()!! == 0) {
                            codePtr = codePtr.copy(offset = codePtr.offset + instr.op.offset)
                        }
                    }
                    is Op.Here -> {
                        // This is a noop word for debugging
                    }
                    is Op.Literal -> {
                        stack.push(instr.op.value)
                    }
                    is Op.LiteralFromStack -> {
                        if (mode != VmMode.COMPILING) {
                            throw Exception("COMPILE ONLY WORD")
                        }

                        if (stack.size() < 1) {
                            throw Exception("STACK UNDERFLOW")
                        }

                        val latestAddr = latestWordAddr!!
                        val latestExecToken = dict.words[latestAddr].code
                        val top = stack.pop()!!
                        latestExecToken.add(Instruction.native(Op.Literal(top)))
                    }
                    is Op.Define -> {
                        if (mode != VmMode.INTERPRETING) {
                            throw Exception("INTEPRET ONLY WORD")
                        }

                        val wordName = parser.consumeWord()
                        if (wordName == null) {
                            throw Exception("EXPECTED WORD NAME")
                        }

                        mode = VmMode.COMPILING
                        latestWordAddr = dict.words.size
                        dict.words.add(Word.new(wordName, mutableListOf()))
                    }
                    is Op.EndDefine -> {
                        val latestAddr = latestWordAddr!!
                        val latestExecToken = dict.words[latestAddr].code
                        latestExecToken.add(Instruction.native(Op.Return()))
                        mode = VmMode.INTERPRETING
                    }
                    is Op.InterpretMode -> {}
                    is Op.EndInterpretMode -> {}
                    is Op.ParseWord -> {
                        throw Exception("Didn't feel like getting string alloc working yet")
                    }
                    is Op.Find -> {
                        throw Exception("Didn't feel like getting string alloc working yet")
                    }
                }
            }
            codePtr = codePtr.copy(offset = codePtr.offset + 1)
        }
    }

    fun interpret() {
        while (true) {
            if (inputExhausted()) {
                parser = getUserInput()
            }

            val textWord = parser.consumeWord()!!

            val wordAddr = dict.lookupWord(textWord)
            if (wordAddr != null) {
                val isImmediate = dict.words[wordAddr].immediate
                if (mode == VmMode.INTERPRETING || isImmediate) {
                    codePtr = CodePtr.startOf(wordAddr)
                    execute()
                } else {
                    val latestWordAddr = latestWordAddr!!
                    val latestExecToken = dict.words[latestWordAddr].code
                    latestExecToken.add(Instruction.user(wordAddr))
                }
                continue
            }

            val parsedNum = textWord.toIntOrNull()
            if (parsedNum != null) {
                when (mode) {
                    VmMode.INTERPRETING -> {
                        stack.push(parsedNum)
                    }
                    VmMode.COMPILING -> {
                        val latestWordAddr = latestWordAddr!!
                        val latestExecToken = dict.words[latestWordAddr].code
                        latestExecToken.add(Instruction.native(Op.Literal(parsedNum)))
                    }
                }
                continue
            }
            // NB: The stack is not reset.
            println("Error: Unrecognized word.")
            parser = Parser("")
        }
    }
}
