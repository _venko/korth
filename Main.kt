import machine.Vm

fun main() {
    val vm = Vm()
    vm.interpret()
}
