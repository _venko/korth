package stack

import java.util.ArrayDeque

class Stack<T>() {
    val items = ArrayDeque<T>()

    fun size(): Int = items.size

    fun isEmpty(): Boolean = items.isEmpty()

    fun clear() = items.clear()

    fun push(item: T) = items.push(item)

    fun pop(): T? = items.pop()

    fun top(): T? = items.peek()
}
