package types

typealias VmWord = Int

typealias WordAddr = Int
typealias StrAddr = Int
typealias StrLen = Int

data class CodePtr(val addr: WordAddr, val offset: Int) {
    companion object Init {
        fun default() = CodePtr(0, 0)

        fun startOf(index: WordAddr) = CodePtr(index, 0)
    }
}
